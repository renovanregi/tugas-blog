<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function Register()
    {
        return view('halaman.Register');
    }

    public function Kirim(Request $request)
    {
        $Name = $request['Name'];
        $Address = $request['Address'];
        $Email = $request['Email'];

        return view('halaman.welcome', compact('Name', 'Address', 'Email'));
    }  
}
